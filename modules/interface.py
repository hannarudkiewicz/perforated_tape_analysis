import os
import os.path
import glob


class UserInterface:

    @staticmethod
    def show_possible_assets():
        print("Suggested paths: ")
        print("./assets/your_directory")
        print("Possible directories: ")
        print(os.listdir("./assets"))
        return

    # check if there is jpg and mov
    @staticmethod
    def check_if_correct(_path):
        path_list = _path.split('/')
        dir_name = path_list[-1]
        if os.path.exists(_path) and os.path.isdir(_path):
            os.chdir(_path)
            jpg_file = glob.glob('*jpg')
            if jpg_file:
                return True, dir_name
            else:
                print("Missing jpg file to analyse")
                return False, dir_name
        else:
            print("Given directory doesn't exist")
            return False, dir_name

    def get_dir_path(self):
        correct = False
        # default
        _path = "./assets"
        while not correct:
            _path = input("Insert directory path: ")
            correct, _dir_name = self.check_if_correct(_path)
            if not correct:
                self.show_possible_assets()
        _files_list = glob.glob("*.jpg")
        _files_list = sorted(_files_list)
        _dir_name = _dir_name + ".txt"
        return _files_list, _dir_name

    # trash
    @staticmethod
    def save_as():
        _save_path = 'results/'
        _results_name = input("Save detected values as: ")
        _results_file = os.path.join(_save_path, _results_name+".txt")
        return _results_file









