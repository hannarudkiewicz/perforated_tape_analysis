import os
import json
import math


class FileCreator:
    # directory - config
    CONFIG_DIR = os.path.join(os.path.dirname(os.path.dirname(__file__)), 'config')

    def __init__(self, config_json_name):
        with open(os.path.join(self.CONFIG_DIR, config_json_name)) as f:
            config = json.load(f)

        self._title = config['title']
        self._fcm = config['fcm']
        self._cut = config['cut']
        self._sop = config['sop']
        self._sat = config['sat']

    @staticmethod
    def frames_to_timecode(_frames):
        h = int(_frames / 86400)
        m = int(_frames / 1440) % 60
        s = int((_frames % 1440) / 24)
        f = _frames % 1440 % 24
        timecode = '%02d:%02d:%02d:%02d' % (h, m, s, f)
        return timecode

    @staticmethod
    def timecode_to_frames(_timecode):
        fps = 24
        frames = sum(f * int(t) for f, t in zip((3600 * fps, 60 * fps, fps, 1), _timecode.split(':')))
        return frames

    @staticmethod
    def shot_duration(_frames_array, _idx):
        duration = _frames_array[_idx + 1] - _frames_array[_idx]
        return duration

    def stop_tc(self, _start_tc, _duration):
        stop_tc = self.frames_to_timecode(self.timecode_to_frames(_start_tc) + _duration)
        return stop_tc

    # TYPES OF COLOR VALUE INTERPRETATIONS - FOR TESTING
    # change color value to float v1
    @staticmethod
    def int_to_float(_value):
        value = float((_value-25)/25)
        return value

    # change color value as ln v2
    def color_as_ln(self, _value):
        _value = self.int_to_float(_value)
        value = float(math.log(_value))
        return value

    # change color value as log v3
    def color_as_log(self, _value):
        _value = self.int_to_float(_value)
        value = float(math.log(_value, 10))
        return value

    # change color value as exp v4
    def color_as_exp(self, _value):
        _value = self.int_to_float(_value)
        value = float(math.exp(_value))
        return value

    def create_values(self, _all_color_values):
        _all_color = []
        for rgb in _all_color_values:
            _all_color.append("%.6f" % self.int_to_float(rgb))
        return _all_color

    def print_color_values(self, _all_color, _counter, _file):
        _file.write(self._sop["0"])
        for j in range(3):
            if _counter < len(_all_color):
                _file.write(_all_color[_counter])
            else:
                _file.write("-1.000000")
            if j < 2:
                _file.write(" ")
            _counter += 1
        _file.write(self._sop["1"])
        _file.write(self._sat["0"])
        return _counter

    def create_file_from_json(self, _all_color, _all_fcc_values, _video_metadata):
        file_name = _video_metadata[0] + "_cdl_results.edl"
        # for printing final rgb values
        counter = 3
        f = open(file_name, "w+")
        if os.stat(file_name).st_size == 0:
            title = self._title["0"]
            f.write(title + _video_metadata[0] + ".edl\n")
            fcm = self._fcm["0"]
            f.write(fcm)
        # count second fcc as first for picture
        diff_fcc = _all_fcc_values[1] - 86400

        for idx, i in enumerate(_all_fcc_values):
            if idx == 0:
                continue
            f.write('{:03}'.format(idx) + self._cut["0"])
            if idx == 1:
                start_src = _video_metadata[1]
                duration = self.shot_duration(_all_fcc_values, idx) + 1
                start_tml = self.frames_to_timecode(i - diff_fcc)
            else:
                start_src = stop_src
                duration = self.shot_duration(_all_fcc_values, idx)
                start_tml = stop_tml
            stop_src = self.stop_tc(start_src, duration)
            stop_tml = self.stop_tc(start_tml, duration)
            if self.timecode_to_frames(stop_tml) - 86400 > self.timecode_to_frames(_video_metadata[2]):
                stop_src = self.timecode_to_frames(_video_metadata[1]) + int(_video_metadata[3])
                stop_src = self.frames_to_timecode(stop_src)
                stop_tml = int(_all_fcc_values[0]) + int(_video_metadata[3])
                stop_tml = self.frames_to_timecode(stop_tml)
                f.write(start_src + " " + stop_src + " " + start_tml + " " + stop_tml + "\n")
                _ = self.print_color_values(_all_color, counter, f)
                break
            else:
                f.write(start_src + " " + stop_src + " " + start_tml + " " + stop_tml + "\n")
                counter = self.print_color_values(_all_color, counter, f)
        f.close()
        return






