import cv2
import os
import json
from operator import itemgetter

# NOTE: for every fcc 1hr (86400 frames) is added to start timeline timecode from 01:00:00:00 = standard


class ImgAnalyser:
    # directory - config
    CONFIG_DIR = os.path.join(os.path.dirname(os.path.dirname(__file__)), 'config')

    # config_json_name - klasa DotsAnalyser przyjmuje jako parametr nazwe pliku json z folderu config (parametry)
    def __init__(self, config_json_name):
        self._tape = None
        with open(os.path.join(self.CONFIG_DIR, config_json_name)) as f:
            config = json.load(f)

        self._color_value_key = config['color_value_key']
        self._y_ranges = config['color_ranges_key']
        self._color_type_key = config['color_type_key']
        self._color_type_color = config['color_type_color']

    @staticmethod
    def sort_by_xy(_array):
        _array_sorted = sorted(_array, key=lambda element: (element[0], element[1]))
        return _array_sorted

    def find_y(self, _y):
        y = 0
        col_val = 0
        for key, value in self._color_value_key.items():
            low, high = tuple(self._y_ranges[key])
            if low <= _y <= high:
                col_val = value
                y = low
        return y, col_val

    def all_dots_detector(self, _contours, _hierarchy):
        # empty_image = np.zeros(_img.shape, np.uint8)
        # array for all dots on the tape
        _dots_contours = []
        # find external contours of dots on the tape
        for cInx, cnt in enumerate(_contours):
            if _hierarchy[0][cInx][3] == -1:
                (x, y), radius = cv2.minEnclosingCircle(cnt)
                (x, y) = (int(x), int(y))
                radius = (int(radius))
                _dots_contours.append([x, y, radius])
        # sort by x value of the circle center
        _dots_contours = self.sort_by_xy(_dots_contours)
        return _dots_contours

    def find_start(self, _dots_contours, _is_start):
        _start_indx = 0
        for inx, c in enumerate(_dots_contours):
            low, high = tuple(self._y_ranges["start"])
            if low < c[1] < high:
                _is_start = True
                _start_indx = inx+1
                dif = abs(_dots_contours[_start_indx][0] - _dots_contours[inx][0])
                if dif < _dots_contours[_start_indx][2]:
                    _start_indx += 1
                break
        return _is_start, _start_indx

    def dots_recognizer(self, _dots_contours, _is_start):
        # array for all dots with color data
        _data_contours = []
        _coordinate_contours = []
        start_indx = 0
        # loop over the contours and find circles with color info
        if not _is_start:
            _is_start, start_indx = self.find_start(_dots_contours, _is_start)

        for j in _dots_contours[start_indx:]:
            if j[2] > 12 and j[2] < 18:
                # cv2.circle(tape_oryginal, center, radius, 255, 2)
                # get color_value based on y
                y, col_val = self.find_y(j[1])
                _data_contours.append([j[0], y, 15, col_val])
            elif j[2] > 8 and j[2] < 12:
                _coordinate_contours.append(j)
        return _data_contours, _coordinate_contours, _is_start

    @staticmethod
    def tape_combiner(_all_values, _new_values):
        elem = _new_values[0]
        idx_counter = 0
        for start_idx, com_elem in reversed(list(enumerate(_all_values))):
            if com_elem == elem:
                idx_counter = 1
                com_val_counter = 1
                for i, el in enumerate(_all_values[start_idx+1:]):
                    if not _new_values[idx_counter] == el:
                        break
                    idx_counter += 1
                    com_val_counter += 1
                if com_val_counter > 4:
                    break
        _all_values = _all_values + _new_values[idx_counter:]
        return _all_values

    def image_combiner(self, _all_values, _new_values):
        _all_values = self.sort_by_xy(_all_values)
        _new_values = self.sort_by_xy(_new_values)
        elem = _new_values[0][3]
        idx_counter = 0
        for start_idx, com_elem in reversed(list(enumerate(_all_values))):
            if com_elem[3] == elem:
                idx_counter = 1
                com_val_counter = 1
                for i, el in enumerate(_all_values[start_idx + 1:]):
                    if not _new_values[idx_counter][3] == el[3]:
                        break
                    idx_counter += 1
                    com_val_counter += 1
                if not com_val_counter < 6:
                    break
        _all_values = _all_values + _new_values[idx_counter:]
        return _all_values

    def one_tape_comp(self, _data_contours, _coordinate_contours, _all_tape_values, _synthetic_data, _counter):
        _tape_values = []
        _tape_contours = []
        indx_cnt = 0
        value = 0
        for coordinateCnt in _coordinate_contours:
            for cnt in _data_contours[indx_cnt:]:
                dif = abs(cnt[0] - coordinateCnt[0])
                if dif < coordinateCnt[2]:
                    value += cnt[3]
                    indx_cnt += 1
                    # for synthetic tape
                    _tape_contours.append([30 + _counter * 30, cnt[1], 15, cnt[3]])
                else:
                    # for synthetic tape
                    if value == 0:
                        _counter += 1
                        _tape_contours.append([30 + _counter * 30, cnt[1], 0, 0])
                    _counter += 1
                    # for values
                    _tape_values.append(int(value))
                    value = 0
                    break
        _tape_values.append(int(value))
        if len(_all_tape_values) == 0:
            _all_tape_values = _tape_values
            _synthetic_data = _tape_contours
        else:
            _all_tape_values = self.tape_combiner(_all_tape_values, _tape_values)
            _synthetic_data = self.image_combiner(_synthetic_data, _tape_contours)
        return _all_tape_values, _synthetic_data, _counter

    def comp_analyser(self, _all_tape_values):
        _all_color_values = []
        _all_fcc_values = []
        counter = 0
        is_fcc = True
        fcc = ""
        fcc_counter = 0
        for value in _all_tape_values:
            if counter == 3:
                counter = 0
                continue
            if is_fcc:
                fcc = fcc + str(value)
                fcc_counter += 1
                if fcc_counter == 6:
                    _all_fcc_values.append(int(fcc) + 86400)
                    fcc = ""
                    fcc_counter = 0
                    is_fcc = False
            else:
                _all_color_values.append(int(value))
                if counter == 2:
                    is_fcc = True
            counter += 1
        return _all_color_values, _all_fcc_values

    # trash
    @staticmethod
    def sort_all_contours(_dots_contours):
        # construct the list of bounding boxes and sort them from top to
        # bottom
        bounding_boxes = [cv2.boundingRect(c) for c in _dots_contours]
        (_dots_contours, bounding_boxes) = zip(*sorted(zip(_dots_contours, bounding_boxes),
                                                       key=lambda b: b[1][0], reverse=False))
        return _dots_contours

    @staticmethod
    def split_path(path):
        path_list = path.split(os.sep)
        filename = path_list[-1]
        data = filename.split('.jpg')[0].split('_')
        return data



