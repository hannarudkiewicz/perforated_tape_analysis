from pymediainfo import MediaInfo


class MetadataFinder:

    @staticmethod
    def split_name(_filename):
        name_list = _filename.split('_')
        return name_list[0]

    def get_metadata(self, _filename):
        metadata = MediaInfo.parse(_filename)
        _duration_tc = metadata.tracks[0].other_duration[4]
        _duration_frames = metadata.tracks[0].frame_count
        _start_tc = metadata.tracks[-1].time_code_of_first_frame
        _title = self.split_name(_filename)
        _video_metadata = [_title, _start_tc, _duration_tc, _duration_frames]
        return _video_metadata


