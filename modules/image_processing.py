import cv2
import numpy as np
from imutils.perspective import four_point_transform
import imutils


class ContourDetector:
    def __init__(self):
        self._thresh = None

    def threshold_otsu(self, _image):
        _, _threshed_image = cv2.threshold(_image, 127, 255, cv2.THRESH_OTSU)
        return _threshed_image

    @staticmethod
    def get_contours(_edge_image):
        _, _contours, _hierarchy = cv2.findContours(_edge_image, cv2.RETR_CCOMP, cv2.CHAIN_APPROX_SIMPLE)
        return _contours, _hierarchy

    @staticmethod
    def create_contours(_image, _contours):
        # create empty black mat for contours
        empty_image = np.zeros(_image.shape, np.uint8)

        for contour in _contours:
            epsilon = 0.01 * cv2.arcLength(contour, True)
            approx = cv2.approxPolyDP(contour, epsilon, True)
            cv2.drawContours(empty_image, [approx], -1, 255, 2)

        element = cv2.getStructuringElement(cv2.MORPH_CROSS, (3, 3))
        _contours_image = cv2.erode(empty_image, element)
        return _contours_image


class GeometricalTransform:

    # find contours in the edge map, then initialize
    # the contour that corresponds to the tape
    @staticmethod
    def get_tape_contours(_contours_image):
        _external_contours = cv2.findContours(_contours_image.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        _external_contours = imutils.grab_contours(_external_contours)

        # ensure that at least one contour was found
        if len(_external_contours) > 0:
            # sort the contours according to their size n
            # descending order
            _external_contours = sorted(_external_contours, key=cv2.contourArea, reverse=True)
        return _external_contours

    @staticmethod
    def get_tape_vertices(_external_contours):
        _tape_vertices = None
        # loop over the sorted contours, look for vertices
        for c in _external_contours:
            # approximate the contour
            peri = cv2.arcLength(c, True)
            approx = cv2.approxPolyDP(c, 0.02 * peri, True)

            # if approximated contour has four points,
            # then it is assumed that it is the tape contour
            if len(approx) == 4:
                _tape_vertices = approx
                break
        return _tape_vertices

    @staticmethod
    def rotate_image(_tape_vertices, _image_contours):
        # apply a four point perspective transform to the image
        # to obtain a top-down birds eye view of the paper
        _tape_eroded_rotated = four_point_transform(_image_contours, _tape_vertices.reshape(4, 2))
        # _tape_oryginal_transformed = four_point_transform(_input_image, _tape_vertices.reshape(4, 2))
        return _tape_eroded_rotated # , _tape_oryginal_transformed

    @staticmethod
    def resize_image(_image_rotated):
        _image_resized = imutils.resize(_image_rotated, height=300)
        return _image_resized




