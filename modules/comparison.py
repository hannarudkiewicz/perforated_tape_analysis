# module for comparing ground truth with program results and groud truth with manual users results
# module for creating synthetic tape
import os
import numpy as np
import cv2
import json


class Synthetic:
    CONFIG_DIR = os.path.join(os.path.dirname(os.path.dirname(__file__)), 'config')

    def __init__(self, config_json_name):
        self._tape = None
        with open(os.path.join(self.CONFIG_DIR, config_json_name)) as f:
            config = json.load(f)

        self._color_type_color = config['color_type_color']

    def synthetic_data_continuity(self, _synthetic_data):
        synthetic_data = []
        counter = 0
        for idx, data in enumerate(_synthetic_data):
            if not idx == 0:
                dif = abs(data[0] - _synthetic_data[idx - 1][0])
                if dif == 0:
                    synthetic_data.append([270 + counter * 30, data[1], data[2], data[3]])
                else:
                    counter += 1
                    synthetic_data.append([270 + counter * 30, data[1], data[2], data[3]])
            else:
                synthetic_data.append([270, data[1], data[2], data[3]])
        return synthetic_data

    def synthetic_tape(self, _synthetic_data):
        font = cv2.FONT_HERSHEY_SIMPLEX
        synthetic_data = self.synthetic_data_continuity(_synthetic_data)
        synthetic_image = np.zeros((300, len(_synthetic_data)*30, 3), np.uint8)
        col_index = 0
        ref_idx = 0
        fcc_x = 0
        key = json.dumps(col_index)
        col_value = self._color_type_color[key]
        for idx, cnt in enumerate(synthetic_data):
            if cnt[2] == 15:
                if not ref_idx == 0:
                    if not cnt[0] == synthetic_data[ref_idx][0]:
                        if col_index == 2:
                            fcc_x = synthetic_data[ref_idx][0]
                        if not col_index == 3:
                            col_index += 1
                        elif cnt[0] - fcc_x > 240:
                            col_index = 0
                        key = json.dumps(col_index)
                        col_value = self._color_type_color[key]
                        cv2.circle(synthetic_image, (cnt[0], cnt[1]), cnt[2],
                                   (col_value['0'], col_value['1'], col_value['2']), 0)
                        cv2.putText(synthetic_image, str(cnt[3]), (cnt[0]-5, cnt[1]+5), font, 0.5,
                                    (col_value['0'], col_value['1'], col_value['2']), 1, cv2.LINE_AA)
                        ref_idx = idx
                    else:
                        cv2.circle(synthetic_image, (cnt[0], cnt[1]), cnt[2],
                                   (col_value['0'], col_value['1'], col_value['2']), 0)
                        cv2.putText(synthetic_image, str(cnt[3]), (cnt[0] - 5, cnt[1] + 5), font, 0.5,
                                    (col_value['0'], col_value['1'], col_value['2']), 1, cv2.LINE_AA)
                        ref_idx = idx
                else:
                    cv2.circle(synthetic_image, (cnt[0], cnt[1]), cnt[2],
                               (col_value['0'], col_value['1'], col_value['2']), 0)
                    cv2.putText(synthetic_image, str(cnt[3]), (cnt[0] - 5, cnt[1] + 5), font, 0.5,
                                (col_value['0'], col_value['1'], col_value['2']), 1, cv2.LINE_AA)
                    ref_idx = idx
            else:
                continue
        cv2.imwrite("tape.png", synthetic_image)
        return


class Comparator:
    GROUND_DIR = os.path.join(os.path.dirname(os.path.dirname(__file__)), 'config/groundtruth')

    @staticmethod
    def detected_results(_all_tape_values):
        f = open("detected_results.txt", "a+")
        for i in _all_tape_values:
            f.write(str(i))
        return f.name

    def compare_with_groundtruth(self, _all_tape_values, _dir_name):
        detected_file = self.detected_results(_all_tape_values)
        f = open(detected_file, "r")
        gt = open(os.path.join(self.GROUND_DIR, _dir_name), "r")
        correct = False
        for line1 in f:
            for line2 in gt:
                if line1 == line2:
                    correct = True
        print(correct)
        f.close()
        gt.close()
        return








