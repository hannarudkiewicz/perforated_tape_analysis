# import the necessary packages
import cv2
import glob
import os
from modules import ContourDetector
from modules import GeometricalTransform
from modules import ImgAnalyser
from modules import FileCreator
from modules import MetadataFinder
from modules import UserInterface
import numpy as np
import imutils

##
# KS: consider adding *.json configs into config dir; creating directory results
# KS: consider adding *.json configs and *.csv for samples

if __name__ == '__main__':
    START = False
    # for reference between tape image parts
    all_tape_values = []
    # modules:
    detector = ContourDetector()
    transformator = GeometricalTransform()
    analyser = ImgAnalyser('dots_analyser_simple.json')
    creator = FileCreator('results.json')
    finder = MetadataFinder()
    interface = UserInterface()

    path = interface.get_dir_path()

    os.chdir(path)
    for image in glob.glob("*.jpg"):
        # load the image in grayscale
        input_image = cv2.imread(image, cv2.IMREAD_GRAYSCALE)
        # process image for geometrical transform
        # make binary image
        threshed_image = detector.threshold_otsu(_image=input_image)
        # find all contours in threshed image
        all_contours, _ = detector.get_contours(_edge_image=threshed_image)
        # find and create contours in the image
        contours_image = detector.create_contours(_image=threshed_image, _contours=all_contours)
        external_contours = transformator.get_tape_contours(_contours_image=contours_image)
        tape_vertices = transformator.get_tape_vertices(_external_contours=external_contours)
        tape_rotated_image = transformator.rotate_image(_tape_vertices=tape_vertices,
                                                        _image_contours=contours_image)
        tape_resized_image = transformator.resize_image(_image_rotated=tape_rotated_image)
        # find contours in the thresholded image, then initialize
        # the list of contours that correspond to wanted data
        contours, hierarchy = detector.get_contours(_edge_image=tape_resized_image)

        dots_contours = analyser.all_dots_detector(_contours=contours, _hierarchy=hierarchy)

        data_contours, coordinate_contours, START = analyser.dots_recognizer(_dots_contours=dots_contours,
                                                                             _is_start=START)

        all_tape_values = analyser.one_tape_comp(_data_contours=data_contours, _coordinate_contours=coordinate_contours,
                                                 _all_tape_values=all_tape_values)

    video_metadata = finder.get_metadata(glob.glob("*mov")[0])
    all_color_values, all_fcc_values = analyser.comp_analyser(_all_tape_values=all_tape_values)

    # all_tc = creator.create_timecodes(_all_fcc_values=all_fcc_values)
    all_color = creator.create_values(_all_color_values=all_color_values)
    # creator.create_file(all_color, all_tc, "krzyz")
    creator.create_file_from_json(_all_color=all_color, _all_fcc_values=all_fcc_values, _video_metadata=video_metadata)
    # creator.create_file_test()
    cv2.destroyAllWindows()

    # trash

    # all_tape_values, all_color_values, all_fcc_values = analyser.comp_analyser(_data_contours=data_contours,
    # _coordinate_contours=coordinate_contours,
    # _all_color_values=all_color_values,
    # _all_fcc_values=all_fcc_values,
    # _all_tape_values=all_tape_values)

    # analyser.color_recogniser(_data_contours=data_contours)
