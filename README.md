﻿assets - image, video files
config - json files with database and used parameters
example - python scripts for testing
misc - txt files for questions and reports
modules - .py files with classes
results - json, csv files with results
sandbox - master python script 
